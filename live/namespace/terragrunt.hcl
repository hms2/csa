terraform {
  # Import module from repository
  source = "git::https://gitlab.com/hms2/infrastructure.git//modules/namespace"
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
# when the bucket not exist terragrunt will suggest to create new one
remote_state {
  backend = "s3"
  config  = {
    bucket         = "csa-bucket-project"
    key            = "terraform/stage/k8s/namespace/namespace2.state"
    region         = "us-east-1"
  }
}

# namespace inputs
inputs = {
  namespace_name    = "production"
  annotations_name  = "terraform-annotation"
  label             = "dev" 
}






