terraform {
  # Import module from repository
  source = "git::https://gitlab.com/hms2/infrastructure.git//modules/hello-world-app"
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
# when the bucket not exist terragrunt will suggest to create new one
remote_state {
  backend = "s3"
  config  = {
    bucket         = "csa-bucket-project"
    key            = "terraform/stage/k8s/deployment/csa/csa.state"
    region         = "us-east-1"
  }
}

# Backend deployment inputs
inputs = {
    app_name       = "csa"
    namespace      = "production"

    #labels
    labels_app     = "hello-world"
    labels_role    = "none"
    labels_tier   = "frontend"
    replicas       = 1

    #selectors
    selector_app   = "hello-world"
    selector_role  = "none"
    selector_tier  = "frontend" 

    #template
    image          = "registry.gitlab.com/hms2/csa:latest"
    container_name = "csa"
    container_port = 3000
    env_name       = "GET_HOSTS_FROM"
    env_value      = "dns"
    
    #resources
    cpu            = "100m"
    memory         = "100Mi"

    #services
    type           = "LoadBalancer"
    port           = 80
    target_port    = 3000

}





